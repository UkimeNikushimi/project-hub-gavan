//  Copyright © 2017 Outware Mobile. All rights reserved.

final class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()

    let label: UILabel = .init()
    label.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(label)

    let centering: [NSLayoutConstraint] = [
      NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 1.0),
      NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1.0, constant: 1.0),
    ]

    NSLayoutConstraint.activate(centering)

    label.reactive.text <~ SignalProducer(value: "Hello world!")
  }

}

import ReactiveCocoa
import ReactiveSwift
import UIKit
