# Project Hub

A playground for Hubbing all the Projects.

## Developer Setup

### Environment

Project Hub uses:

[Homebrew](http://brew.sh) to manage environment dependencies.

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

[Carthage](https://github.com/Carthage/Carthage) to manage project dependencies.

```
brew update && brew install carthage
```

[SwiftLint](https://github.com/realm/SwiftLint) to lint project source.

```
brew install swiftlint
```

### Project

After cloning the project, prepare the project.

- Build the dependencies.
- Install Xcode templates.

```
rake project:setup
```

Once set-up, run the project's tests to ensure that everything is currently okay.

```
rake project:test
```

The `project:test` task also runs the (unreliable) feature (UI) tests. If desired, test _only_ the units of the project. (For example, if the UI tests are causing unexplained test failures)

```
rake project:test:units
```

## Development

- As part of the development setup, the `rake project:setup` task installs an Xcode template, grouped under "Clean".
    - Use this template where possible for consistency across the project.
