//  Copyright © 2017 Outware Mobile. All rights reserved.

/// A convenience central proxy referencing the application,
/// for use in feature test classes at feature testing time.
let application: XCUIApplication = .init()

class QuickUISpec: QuickSpec {

  override func setUp() {
    super.setUp()
    continueAfterFailure = false
    application.launch()
  }

}

import Quick
