//  Copyright © 2017 Outware Mobile. All rights reserved.

final class Features: QuickUISpec {

  override func spec() {

    describe("the application's first screen") {

      it("greets the user with 'Hello world!'") {
        let greeted: Bool = application.staticTexts["Hello world!"].exists
        expect(greeted).to(beTrue())
      }
    }

  }

}

import Nimble
import Quick
