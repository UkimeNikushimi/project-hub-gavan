//  Copyright © 2017 Outware Mobile. All rights reserved.

final class Specs: QuickSpec {

  override func spec() {

    describe("imminent success") {

      it("results in success") {
        let imminentSuccess: (() -> ToSucceedResult) = { .succeeded }
        expect(imminentSuccess).to(succeed())
      }

      context("sent asynchronously") {

        it("results in success") {
          let asyncImminentSuccess: SignalProducer<ToSucceedResult, NoError> = SignalProducer(value: .succeeded).delay(2.0, on: QueueScheduler.main)
          let container: Property<ToSucceedResult?> = Property(initial: nil, then: asyncImminentSuccess.map(Optional.init))

          expect(container.value).toEventuallyNot(beNil())
        }

      }

    }

  }

}

@testable import Project_Hub

import Nimble
import Quick

import ReactiveSwift
import enum Result.NoError
